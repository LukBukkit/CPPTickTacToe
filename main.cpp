#include <iostream>
#include <limits>
#include "TicTacToeField.h"

int readInt();

using namespace std;

int main() {

    cout << "Willkommen bei TicTacToe in C++! Es gibt 2 Spiele, welche abwechselnd ein Feld auswählen können.\n"
            "Das Symbol \"O\" steht für den ersten Spieler, das Symbol \"X\" für den Zweiten.\nViel Spaß!\n";

    TicTacToeField field;

    int currplayer = 1;
    bool alertmsg = true;

    while (!field.hasWinner()) {
        if (alertmsg) {
            cout << "Spieler " << currplayer << " ist am Zug! Wähle bitte ein Feld aus!\n";
            alertmsg = false;
        }

        cout << "Spalte(↓): \n";
        int column = readInt();
        if (!column) continue;

        cout << "Zeile(→): \n";
        int row = readInt();
        if (!row) continue;

        int ifield = (column - 1) * 3 + (row - 1);

        if (field.getField(ifield) != 0) {
            cout << "Das Feld <" << column << "," << row << "> ist schon belegt!" << endl;
            continue;
        }

        field.setField(ifield, currplayer);

        field.render();

        alertmsg = true;

        if (currplayer == 2) {
            currplayer = 1;
        } else {
            currplayer += 1;
        }
    }

    return 0;
}

int readInt() {
    int read;
    cin >> read;
    if (!read) {
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << "Du musst eine Zahl eingeben!" << endl;
        return false;
    }
    if (1 > read || read > 3) {
        cout << "Du musst eine Zahl zwischen 1 und 3 angeben!";
        return false;
    }
    return read;
}