#ifndef CFUN_TICTACTOEFIELD_H
#define CFUN_TICTACTOEFIELD_H

class TicTacToeField {
private:
    int * fields;
    int check();
    bool winner;
public:
    TicTacToeField();
    virtual ~TicTacToeField();

    void render();
    void setField(int field, int player);
    int getField(int field);
    bool hasWinner();
};


#endif
