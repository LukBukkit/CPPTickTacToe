#include <iostream>
#include "TicTacToeField.h"

using namespace std;

const int FIELD_COUNT = 9;

TicTacToeField::TicTacToeField() {
    fields = new int[FIELD_COUNT];
    winner = false;
}

TicTacToeField::~TicTacToeField() {
    fields = NULL;
    winner = NULL;
}

void TicTacToeField::setField(int field, int player) {
    fields[field] = player;
}

int TicTacToeField::getField(int field) {
    return fields[field];
}

void TicTacToeField::render() {

    for (int i = 0; i < FIELD_COUNT; i++) {
        string out;
        switch (fields[i]) {
            case 1:
                out = "O";
                break;
            case 2:
                out = "X";
                break;
            default:
                out = "-";
                break;
        }
        cout << out;

        if ((i + 1) % 3 == 0) {
            cout << "\n";
        }
    }

    string out2;

    for (int i = 0; i < 10; i++) {
        out2 += "=";
    }

    cout << out2;
    cout << "\n";

    int checkval = check();

    if (checkval != -1) {
        winner = true;
        cout << "Der Spieler " << checkval << " hat das Spiel gewonnen!\n";
        return;
    }
}

int TicTacToeField::check() {

    // Zeilen
    for (int i = 0; i < 3; i++) {
        int owner[] = {-1, -1, -1};

        for (int i2 = 0; i2 < 3; i2++) {
            int field = i * 3 + i2;
            owner[i2] = getField(field);
        }

        if (owner[0] == 0) continue;

        if (owner[0] == owner[1] && owner[0] == owner[2]) return owner[0];
    }

    // Spalten
    for (int i = 0; i < 3; i++) {

        int owner[] = {-1, -1, -1};

        for (int i2 = 0; i2 < 3; i2++) {
            int field = 3 * i2 + i;
            owner[i2] = getField(field);
        }

        if (owner[0] == 0) continue;

        if (owner[0] == owner[1] && owner[0] == owner[2]) return owner[0];
    }

    // Quer

    if (getField(4) != 0) {
        if (getField(0) == getField(4) && getField(0) == getField(8)) {
            return getField(0);
        } else if (getField(2) == getField(4) && getField(2) == getField(6)) {
            return getField(2);
        }
    }

    return -1;
}

bool TicTacToeField::hasWinner() {
    return winner;
}









